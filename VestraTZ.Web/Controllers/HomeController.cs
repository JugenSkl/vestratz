﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VestraTZ.Web.db;
using VestraTZ.Web.Models;

namespace VestraTZ.Web.Controllers
{
    public class HomeController: Controller
    {
        private SQLiteContext _context;

        public HomeController(SQLiteContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var x = _context.Orders.ToList();
            return View(x);
        }

        [HttpGet]
        public IActionResult NewOrder()
        {
            return View();
        }

        [HttpPost]
        public IActionResult NewOrder(Order order)
        {
            if (ModelState.IsValid)
            {
                var item = _context.Orders.Add(order);
                _context.SaveChanges();
                return RedirectToAction("OrderСreated", "Home", item.Entity);
            }       
            else
            {
                return View();
            }     
        }

        public IActionResult OrderСreated(Order order)
        {
            return View(order);
        }
    }
}
