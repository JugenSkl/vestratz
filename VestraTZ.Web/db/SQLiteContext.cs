﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VestraTZ.Web.Models;

namespace VestraTZ.Web.db
{
    public class SQLiteContext : DbContext
    {
        #region Constructors
        public SQLiteContext()
        {
            string folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "VTZ");
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            _baseAddress = Path.Combine(folder, "VZTdb.db");

            Database.EnsureCreated();
        }

        public SQLiteContext(string baseAddress)
        {
            Database.EnsureCreated();
            _baseAddress = baseAddress;
        }
        #endregion

        #region Properties
        public DbSet<Order> Orders { get; set; }
        #endregion

        private string _baseAddress;

        #region Methods
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source={_baseAddress};");
        }
        #endregion
    }
}
