﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VestraTZ.Web.Models
{
    public class Order
    {
        public int Id { get; set; }

        [Required (ErrorMessage = "Не указан город отправки")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string SenderCity { get; set; }

        [Required (ErrorMessage = "Не указан адрес отправки")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string SenderAddress { get; set; }

        [Required (ErrorMessage = "Не указан город доставки")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string RecipientCity { get; set; }

        [Required (ErrorMessage = "Не указан адрес доставки")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string ReceiverAddress { get; set; }

        [Required (ErrorMessage = "Не указан вес груза")]
        [Range(0.01, 1000, ErrorMessage = "Вес груза должен быть в интервале от 0.01 до 1000.")]
        public float ShipmentWeight { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Введите дату")]
        public DateTime PickupDate { get; set; }
    }
}
